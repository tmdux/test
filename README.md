# Getting Started #
This design system is focused on enterprise services that want their products to be usable and an enjoyable experience that is consistent with the Rakuten brand. This was created by the TMD UX Design team. If you have questions or need more customized designs please reach out. 

## Table of Contents ##
* --
* --

## Who is this for? ##
Teams that are looking to skin an external service
Teams that are creating services from scratch and need a starting point

## Who is currently using it? ##

## Support ##
* Repo owner or admin
* Other community or team contact

# Documentation #
CSS and sketch overview and link

## Library ##
### Elements & Patterns ###
### Visual Specs ###

## Guidelines & Situations ##
The library guides you to select the best UI solution for your situation (context and action) which will allow us to be consistent for common situations. These are not rules but guidelines.

### Feedback ###

## Templates ##
### Basic Template ###
### More coming soon... ###